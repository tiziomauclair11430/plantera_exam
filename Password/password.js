let check = function () {
  if (
    document.getElementById("password-1").value ==
    document.getElementById("password-2").value
  ) {
    document.getElementById("formSubmit").disabled = false;
    document.getElementById("formSubmit").style.background = "blue";
    document.getElementById("message").style.color = "green";
    document.getElementById("message").innerHTML = "Mot de passe correspondant";
  } else {
    document.getElementById("formSubmit").disabled = true;
    document.getElementById("formSubmit").style.background = "grey";
    document.getElementById("message").style.color = "red";
    document.getElementById("message").innerHTML =
      "Mot de passe ne correspondant pas";
  }
};
let validate = function () {
  console.log(document.getElementById("password-1").value);
  console.log(document.getElementById("password-2").value);
  if (document.getElementById("password-1").value.length < 5) {
    document.getElementById("pwd-length-1").innerHTML = "Au moins 6 caractères";
  } else {
    document.getElementById("pwd-length-1").innerHTML = "";
    check();
  }
  if (document.getElementById("password-2").value.length < 5) {
    document.getElementById("pwd-length-2").innerHTML = "Au moins 6 caractères";
  } else {
    document.getElementById("pwd-length-2").innerHTML = "";
    check();
  }
};
