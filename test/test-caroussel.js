function ajout() {
  if (document.getElementsByName("div").length <= 7) {
    document.getElementById("div").innerHTML += `
        
        <div class="profile-pic" name="div">
        <input onclick="supp(

        )" value="-">
        <label class="-label" name="label" for="file${
          document.getElementsByName("div").length
        }">
            <span class="glyphicon glyphicon-camera"></span>
            <span>Change Image</span>
        </label>
        
        <input name="file" id="file${
          document.getElementsByName("div").length
        }" class="input-file" type="file" onchange="previewPicture(this,${
      document.getElementsByName("div").length
    })">

            <img name="img${
              document.getElementsByName("div").length
            }" class="rendu-image" for="file">
        
    </div>
        `;

    console.log(tab);
  } else {
    alert("vous ne pouvez pas ajouter plus d'image");
  }
}
function supp() {
  document.getElementById("div").innerHTML = `
  <div class="plus">
            <button type="button" onclick="ajout()" class="button-plus">+</button>
    </div>
  `;
}

// La fonction previewPicture
var previewPicture = function (e, place) {
  // e.files contient un objet FileList
  const [picture] = e.files;
  // "picture" est un objet File
  if (picture) {
    // L'objet FileReader
    var reader = new FileReader();

    // L'événement déclenché lorsque la lecture est complète
    reader.onload = function (e) {
      // On change l'URL de l'image (base64)
      let image = document.getElementsByName("img" + place)[0];
      image.style.width = "100%";
      image.style.height = "100%";
      image.src = e.target.result;
    };

    // On lit le fichier "picture" uploadé
    reader.readAsDataURL(picture);
  }
};
