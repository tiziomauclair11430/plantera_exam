<?php session_start();
if (isset($_SESSION['panier'])) {
    include '../header/header.php';

?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Test</title>
</head>

<body>
    <h1>Votre panier</h1>

    <div class="info">
        <p>Voulez-vous vous identifier a votre Compte ou en créer un <a href="../sign-up-login/Sing-up-login.php">suivez
                ce lien </a>!!!</p>
    </div>

    <div class="cart">
        <div class="shopping-cart">

            <div class="column-labels">
                <label class="product-image">Image</label>
                <label class="product-details">Product</label>
                <label class="product-price">Price</label>
                <label class="product-quantity">Quantity</label>
                <label class="product-removal">Remove</label>
            </div>

            <?php
//dans notre data.php c’est ici on se trouve toutes les fonctions qui vont chercher dans la base de données 
               include "../BDD/data.php";
               $panier = $_SESSION['panier'];
//le premier total sera le total de tous les prix sans solde 
               $total = 0;
//le second total sera le total de tous les prix soldé
               $total2 = 0;
               $i = 0;
//Pour le panier nous faisons un foreach qui va boucler sur chaque élément du $_SESSION[‘panier’]
               foreach ($panier as $produit) {
//id du produit selectionné dans la boutique 
                   $id = $produit['id'];
//la quantité selectionné 
                   $quantite = $produit['quantite'];
//ici notre fonction qui va chercher les produit reliée a l’id dans la BDD
                   $select = getProduitById($id)[0];
//si dans la base de données le produit n’a pas de solde alors 
                       if($select['si_solde'] == 1){
//cette variable calcul la quantité prise dans le panier et prend le prix du produit, on multipli le prix par rapport a la quantitée prise 
                           $prixUnProduit = $quantite * $select['prix'];
//cette variable va servir a stockée les prix du foreach
                           $total += $prixUnProduit;
//cette variable va juste ajouter 0 si le produit n’a pas de solde
                           $prixUnProduit2 = 0;
//sinon dans la base de données le produit a une solde alors   
                       }else if($select['si_solde'] == 0) {
//cette variable calcul la quantité prise dans le panier et prend le prix soldée du produit, on multipli le prix par rapport a la quantitée prise 
                           $prixUnProduit2 = $quantite * $select['prix_solde'];
//cette variable va servir a stockée les prix soldée du foreach
                           $total2 += $prixUnProduit2;
//cette variable va juste ajouter 0 si le produit a une solde
                           $prixUnProduit = 0;
                       }
//ces variable va voir si le produit est en livraison ou retrait en magasin
                   $livr = $produit['si_livraison'];
                   $retrait = $produit['si_retrait'];
//cette variable va faire le total de tout les produit dans le panier 
                   $globalTotal =  $total + $total2;
               ?>
            <form action="../controller/Redirect-Stripe.php" method="post">
                <div class="product">
                    <div class="product-image">
                        <img src="../stock/uploads/<?php echo getimage($select['id'])[0]['url'] ?>" class="product-image">
                    </div>
                    <div class="product-details">
                        <div class="product-title" name="product-title"><?php echo $select['nom'] ?></div>
                        <p class="product-description"><?php echo $select['descriptionMini'] ?></p>
                    </div>
                    <div class="product-price">
                        <?php if($select['si_solde'] == 1){
                            print_r ('<p class="parag flex-row" style="justify-content:flex-start;"><span name="prix">'.$select['prix'].'</span>$</p>');
                        }else{
                            print_r ('<h6  style="text-decoration-line: line-through;">'.$select['prix'].'$</h6>') & print_r ('<h6 style="color:red;justify-content: flex-start;" class="flex-row"><span name="prix">'.$select['prix_solde'].'</span>$</h6>');}?>
                    </div>

                    <div class="product-quantity">
                        <input type="number" value="<?php echo $quantite ?>" min="1" onchange="change()"
                            name="quantity<?php echo $i?>" max="<?= $select['stock'] ?>">
                        <input type="hidden" name="id<?php echo $i?>" value="<?php echo $id?>">
                        
                    </div>
                    <div class="product-removal">
                        <a href="../controller/deletepanier.php?action=delete&id=<?php echo $select["id"]; ?>"><span
                                class="text-danger">Remove</span></a></td>
                    </div>

                </div>

                <?php $i++; } ?>
        </div>
        <div id="centrecontainer">
            <p id="parag"></p>
            <div class="flex-columns" id="recap">
                <p><?php echo $select['nom'] ?></p>
                <p>x <?php echo $quantite ?></p>
            </div>
            <div class="flex-row border-top">
                <p class="total">Total:</p>
                <p id="cart-subtotal" class="totals-value"><?php echo $globalTotal ?><p>
                
            </div>
            <div class="button">
                <input type="hidden" name="livr" value="<?php echo $livr ?>">
                <input type="hidden" name="retrait" value="<?php echo $retrait ?>">
                <button href="" type="submit" value="submit" name="submit" class="btn">Valider mon panier </button>
                </form>
            </div>
        </div>

    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="app.js"></script>
    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
</body>
<?php } else {
    echo'
    <link rel="stylesheet" href="styles.css">
    <body id = "bodyOfPaniervide">
    <script>
  document.body.style.zoom=0.8;
</script>
    <div class="cart-empty">
    <div class="empty-cart-header">
    </div>
        <div class="empty-cart">
            <a href="../boutique/boutique.php"><span class="empty-cart-icon"><img src = "https://www.agnassan.com/images/empty.png"></span></a>
            <h2 class = "h2RED">VOTRE PANIER EST ACTUELLEMENT VIDE!</h2>
          <form method="get" action="../boutique/boutique.php">
    <button class ="buttonContinuer" type="submit">Continuer vos achats</button>
</form>
        </div>
 </div>
 </body>
 ';

} ?>

</html>
