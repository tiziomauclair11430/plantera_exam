<?php 
require __DIR__.'/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

$html = '
<style>

body{
    display:flex;
    flex-direction:columns;
    
img{
    width:auto;
    height:100px;
}
.header{
    width:auto;
    height:auto;
    display:flex;
    flex-direction: row;
    margin:0;
}
table
{
    width:  80%;
    border:none;
    border-collapse: collapse;
    margin: auto;
}
th
{
    text-align: center;
    border: solid 1px #eee;
    background: #f8f8f8;
}
td
{
    text-align: center;
    border: solid 1px #eee;
}
</style>
<div class="header">
<img src="../stock/ress/Logo_Plantera.png">
<h1>Factures du '.htmlspecialchars($_POST['created']).'</h1>
</div>

<p>Pseudo : '.htmlspecialchars($_POST['pseudo']).'</p>
<p>Prénom : '.htmlspecialchars($_POST['cust_name']).'</p>
<p>Mail : '.htmlspecialchars($_POST['cust_email']).'</p>
<p>Numéro de téléphone : '.htmlspecialchars($_POST['customerPhone']).'</p>
<h2>Votre commande</h2>
<table>
<col style="width: 33%">
    <col style="width: 33%">
    <col style="width: 33%">
<tr>
<th>Numéro de commande</th>
<th>Montant Payer (hors tva)</th>
<th>Date</th>

</tr>
<tr>
<td>'.htmlspecialchars($_POST['order_number']).'</td>
<td>'.htmlspecialchars($_POST['paid_amount']).'$</td>
<td>'.htmlspecialchars($_POST['created']).'</td>
</tr>
</table>
<h3>Details de votre commande</h3>
<table>
<col style="width: 33%">
    <col style="width: 33%">
    <col style="width: 33%">
<tr>
<th>Numéro de commande</th>
<th>Prix du produit</th>
<th>Nom du produit</th>
<th>Options d"expédition</th>	
</tr>
<tr>
<td>'.htmlspecialchars($_POST['order_number']).'</td>
<td>'.htmlspecialchars($_POST['item_price']).'$</td>
<td>'.htmlspecialchars($_POST['item_name']).'</td>
<td>'.htmlspecialchars($_POST['mode_livraison'] ).'</td>
</tr>
</table>
';

$l = 'livraison';
$html2pdf = new Html2Pdf();
$html2pdf->writeHTML($html);
$html2pdf->output();


?>