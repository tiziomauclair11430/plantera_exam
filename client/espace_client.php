<?php
session_start();
$compte = $_SESSION['compte'];
include '../BDD/data.php';
include '../protected/protectedClient.php';
include '../debug/debug.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espace Client</title>
    <link rel="stylesheet" href="espace_client.css">
</head>

<body>

<script>
  document.body.style.zoom=0.8;
</script>

    <?php foreach (selectClientNom($compte['pseudo']) as $select) { ?>
    <div class="admin-panel clearfix">
        <div class="slidebar">
            <div class="logo">
                <a href="espace_client.php#Modifier-mon-profil">
                    <img id="avatar" src="<?php echo $select['photo_profil'] ?>">
                </a>
            </div>

            <ul>

                <li><a href="#dashboard" id="targeted">Dashboard</a></li>
                <li><a href="#mon-profil">Mon profil</a></li>
                <li><a href="#Modifier-mon-profil">Modifier mon profil</a></li>
                <li><a href="#supprimer-mon-profil">supprimer mon profil</a></li>
                <li><a href="#voir-commande">Voir mes factures</a></li>
                 <li><a href="#historique-de-achats">Mes commandes</a></li>
                <li><a href="../home/index.php">Retour boutique</a></li>
                <li><a href="../sessiondelete/sessiondelete.php">Deconnexion</a></li>

            </ul>
        </div>

        <div class="main">
            <div class="mainContent clearfix">
                <div id="dashboard">
                    <h2 class="header"><span class="icon"></span>Dashboard</h2>
                    <div class="welcome-card">

                        <h1>Bienvenue <span><?php echo $select['pseudo']; ?></span></h1>
                        <p>
                            ✌️Hey, je vous souhaite la bienvenue sur votre espace client.
                        </p>
                    </div>
                </div>
                <div id="mon-profil">
                    <h2 class="header">Mon profil</h2>

                    <div class="container">
                        <div id="client">

                            <p class="nomClient">Nom <strong><?php echo $select['pseudo']; ?></strong></p>
                            <p class="numero">Numero de telephone : <strong><?php echo $select['numero_de_tel']; ?>
                                </strong></p>
                            <p class="adresse">Adresse mail : <strong><?php echo $select['addresse']; ?> </strong></>

                            <form action="../client/espace_client.php#Modifier-mon-profil" method="post">
                                <button type="submit" name="modif" value="<?php echo $select['id'] ?>"><i
                                        class="fas fa-pen"></i>Modifier</button></button>
                            </form>

                        </div><!-- cleint -->
                    </div><!-- /.container -->
                    <?php } ?>
                </div>
                <div id="Modifier-mon-profil">
                    <h2 class="header">Modifier mon profil</h2>
                    <?php foreach (selectClientNom($compte['pseudo']) as $select) { ?>
                    <form class="" action="../controller/updateClient.php" method="post" enctype="multipart/form-data">


                        <div class="profile-pic">
                            <label class="-label" for="file">
                                <span class="glyphicon glyphicon-camera"></span>
                                <span>Change Image</span>
                            </label>
                            <input type="file" name="file" id="file" accept="image/*"
                                onchange="loadFile(event)" />

                            <img id="output" width="200" src="<?php echo $select['photo_profil'] ?>" />

                        </div>

                        <div class="input-flex">
                            <p class="P_profil">Nom:</p>
                            <input type="text" id="nom" name="nom" class="input-product"
                                value="<?php echo $select['pseudo']; ?>">
                        </div>

                        <div class="input-flex">
                            <p class="P_profil">Numero de telephone :</p>
                            <input type="text" id="Numero-de-telephone" name="Numero-de-telephone" class="input-product"
                                value="<?php echo $select['numero_de_tel']; ?>">
                        </div>

                        <div class="input-flex">
                            <p class="P_profil">Adresse mail :</p>
                            <input type="text" id="mail" name="mail" class="input-product"
                                value="<?php echo $select['addresse']; ?> ">
                        </div>

                        <button class="btn" type="submit" name="id" value="<?php echo $select['id']; ?>">Modifier
                        </button>
                    </form>
                    <?php } ?>


                </div>
                <div id="supprimer-mon-profil">
                    <h2 class="header">Supprimer mon profil</h2>
                    <p>Pour supprimer votre compte, remplisser le champ avec votre mots de passe.</p>
                    <form class="form" method="post" action="../controller/deleteAccountClient.php">
                        <input type="hidden" placeholder="Login" value="<?php echo $select['pseudo'] ?>" name="pseudo"
                            id="pseudo">

                        <input type="password" placeholder="Password" required="" name="mdp" id="mdp"
                            class="input-product">
                        <button type="submit" name="supp" id="valid" value="<?php echo $select['id'] ?>"><i
                                class="fas fa-trash"></i></button>
                </div>
                </form>
                <div id="voir-commande">
                    <h2 class="header">Voir mes factures</h2>

                    <?php 
                    foreach (selectClientCommande($select['id']) as $select) {
                  
                        
                    ?>
                    <form action="pdfMaker.php" method="post" enctype="multipart/form">
                        <input type="hidden" name="id_utilisateur" value="<?php echo $select['id_utilisateur']; ?>">
                        <input type="hidden" name="id_transaction" value="<?php echo $select['id_transaction']; ?>">
                        <input type="hidden" name="order_number" value="<?php echo $select['order_number']; ?>">
                        <input type="hidden" name="paid_amount" value="<?php echo $select['paid_amount']; ?>">
                        <input type="hidden" name="created" value="<?php echo $select['created']; ?>">
                        <input type="hidden" name="cust_name" value="<?php echo $select['cust_name']; ?>">
                        <input type="hidden" name="cust_email" value="<?php echo $select['cust_email']; ?>">
                        <input type="hidden" name="customerPhone" value=" <?php echo $select['customerPhone']; ?>">
                        <input type="hidden" name="customerAddress" value="<?php echo $select['customerAddress']; ?>">
                        <input type="hidden" name="mode_livraison" value="<?php echo $select['mode_livraison']; ?>">
                        <input type="hidden" name="mode_retrait" value="<?php echo $select['mode_retrait']; ?>">
                        <input type="hidden" name="item_name" value="<?php echo $select['item_name']; ?>">
                        <input type="hidden" name="item_price" value="<?php echo $select['item_price']; ?>">
                        <input type="hidden" name="order_number" value="<?php echo $select['order_number']; ?>">
                        <input type="hidden" name="pseudo" value="<?php echo $compte['pseudo']; ?>">
                        <label><?php echo $select['created']; ?></label>
                        <button type="submit" class="pdf"><i class="fa-regular fa-file-pdf"></i></button>
                    </form>


                    <?php } ?>

                </div>





                <div id="historique-de-achats">
                    <h2 class="header">Historique de mes achats</h2>
                    <?php 
               
               foreach (selectAllCommandeByidOFClient($select['id']) as $select) {
                        // var_dump($select);
                        echo '<br></br>';
                        
                        ?>
                        
                    <div class="commande">
                        <label for="tableaux-commande-client"><b style="color:gray">Vos coordonnées</b></label>
                        <table id="tableaux-commande-client">
                            <thead>
                                <tr>
                                    <th>Numéro de commande</th>
                                    <th>Nom</th>
                                    <th>Email</th>
                                    <th>Numéro de telephone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b style = "color:red"><?php echo $select['order_number']?></b></td>
                                    <td><b><?php echo $select['cust_name']?></b></td>
                                    <td><b><?php echo $select['cust_email']?></b></td>
                                    <td><b><?php echo $select['customerPhone']?></b></td>
                                </tr>
                            </tbody>
                        </table>
                        <label for="tableaux-commande-client"><b style="color:gray">Produits commander</b></label>
                        <table id="tableaux-commande-client">
                            <thead>
                                <tr>
                                    <th>image</th>
                                    <th>Nom du produit</th>
                                    <th>Prix du produit</th>
                                    <th>Quantity du produit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td><img class="imgProduct" src="../stock/uploads/<?php echo $select['url'] ?>" />
                                        </td>
                                    <td><b><?php echo $select['nom']?></b></td>
                                    <td><b><?php echo $select['item_price']?></b></td>
                                    <td><b><?php echo $select['quantite']?></b></td>
                                </tr>
                            </tbody>
                        </table>
                        <p>Options d'expédition : <b><?php echo $select['mode_livraison']?></b> -  9 rue Planetra</p>
                        <?php 
                        if($select['mode_livraison'] === 'livraison'){?>
                            <p>Adresse d'expédition : <b><?php echo $select['customerAddress'].', '.$select['customerCity'].' '.$select['customerZipcode'].'. '.$select['customerCountry']?></b></p>
                        <?php } ?>
                         
                        <p>Date prise de la commande : <b><?php echo $select['created']?></b></p>
                        <p>Total payé : <b><?php echo number_format($select['paid_amount'])?>$</b></p>

                            
                        <div class=" payment_status_flex">
                            <p>
                                Paiement Accepter :<?php if($select['payment_status'] === "succeeded"){ echo '<div class="payment_status_green"></div>';}else{echo '<div class="payment_status_red"></div>';} ?>
                            </p>
                        </div>
                    </div>


                    <?php } ?>

                </div>




            </div>
        </div>
    </div>

    <script src="app.js"></script>
    <script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
</body>

</html>
