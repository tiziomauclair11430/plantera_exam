<?php 

include '../BDD/data.php';
include '../debug/debug.php';
$error = [];
if($_POST['nom'] !== "" && isset($_POST['nom'])){
    $nom = $_POST['nom'];
}else{
    array_push($error,'Titre de produit manquant');
}
if($_POST['description'] !== "" && isset($_POST['description'])){
    $description = $_POST['description'];
}else{
    array_push($error,'Description du produit manquant');
}
if($_POST['Minidescription'] !== "" && isset($_POST['Minidescription'])){
    $Minidescription = $_POST['Minidescription'];
}else{
    array_push($error,'Mini-Description du produit manquant');
}
if($_POST['temperature'] !== "" && isset($_POST['temperature'])){
    $temperature = $_POST['temperature'];
}else{
    array_push($error,'La temperature du produit manquant');
}
if($_POST['humidite'] !== "" && isset($_POST['humidite'])){
    $humidite = $_POST['humidite'];
}else{
    array_push($error,"L'humidité du produit manquant");
}
if($_POST['temps'] !== "" && isset($_POST['temps'])){
    $temps = $_POST['temps'];
}else{
    array_push($error,'Le temps du produit manquant');
}
if($_POST['stock'] !== "" && isset($_POST['stock'])){
    $stock = $_POST['stock'];
}else{
    array_push($error,'Le stock du produit manquant');
}
if($_POST['prix'] !== "" && isset($_POST['prix'])){
    $prix = $_POST['prix'];
}else{
    array_push($error,'Le prix du produit manquant');
}
    $si_solde = 1;
    $Prixsolde = NULL;
    $livraison = 1;
    if(intval($_POST['si_solde']) === 0){
        $si_solde = 0;
        if($_POST['Prixsolde'] !== "" && isset($_POST['Prixsolde'])){
           $Prixsolde = $_POST['Prixsolde'];  
        }else{
            array_push($error,'Le prix soldée du produit manquant alors que la solde a été choisie');
        }  
    }
    if(isset($_POST['livraison'])){
        $livraison = 0;
    }
    
//     var_dump(
//     "nom => ".$nom,
//     "description => ".$description,
//     "Minidescription => ".$Minidescription,
//     "temperature => ".$temperature,
//     "humidite => ".$humidite,
//     "temps => ".$temps,
//     "stock => ".$stock,
//     "prix => ".$prix,
//     "si_solde => ".$si_solde,
//     "Prixsolde => ".$Prixsolde,
//     "livraison => ".$livraison);
if(count($error) === 0){
        $id_product = addProduit($nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison);
        for($i = 0;$i <= $_POST['nbrImg'];$i++){
            if (move_uploaded_file($_FILES['file'.$i]['tmp_name'], "../stock/uploads/".$_FILES['file'.$i]['name'])) {
                print "Téléchargé avec succès!";
                $id_img = addImage($_FILES['file'.$i]['name']);
                liaison_carousel($id_product,$id_img);
            } else {
                print "Échec du téléchargement de l'image".$_FILES['file'.$i]["name"];
            }
        }
        header('location:../admin/espace_admin.php#voir-produit');
}else{
    echo '
    <div style="display: flex;align-items: center;height: 50px;justify-content: space-evenly;"><h3 style="color:red;font-size:50px">/!\</h3><h1>Error : Données Invalides</h1><h3 style="color:red;font-size:50px">/!\</h3></div>
    <div style="display: flex;flex-direction: column;justify-content: flex-end;margin: 50px;width: 90%;border: solid;padding: 10px;">
    ';
    $i = 0;
    foreach($error as $select){
        echo '<strong style="color:red">Erreur n°'.$i.'</strong><p>'.$select.'</p>';
        $i++;
    }
    echo '</div><a href="'.$_SERVER['HTTP_REFERER'].'#ajouter-produit">retour sur le formulaire</a>';
}

?>