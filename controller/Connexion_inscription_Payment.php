<?php
include "../debug/debug.php";
include '../BDD/data.php';
session_start();


$panier = $_SESSION['panier'];
foreach ($panier as $produit) {
    $livr = $produit['si_livraison'];
    $retrait = $produit['si_retrait'];
}

$pseudo_ins = $_POST["pseudo_ins"];
$mail_ins = $_POST["mail_ins"];
$num_ins = $_POST["num_ins"];
$mdp_ins = $_POST["mdp_ins"];


        $uppercase = preg_match('@[A-Z]@', $mdp_ins);
        $lowercase = preg_match('@[a-z]@', $mdp_ins);
        $number    = preg_match('@[0-9]@', $mdp_ins);
        $specialChars = preg_match('@[^\w]@', $mdp_ins);

// livraison
if ($livr === true ){
    if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($mdp_ins) < 8) {
        header ('location: ../ConnexionPayment/Sing-up-login.php?error4=Le mot de passe doit comporter au moins 8 caractères et doit inclure au moins une lettre majuscule, un chiffre et un caractère spécial.') ;
    }else{
    if (CheckExistClientName($pseudo_ins) === false && CheckExistClientMail($mail_ins) === false && CheckExistClientPhone($num_ins) === false ){
    if ($_POST["pseudo_ins"] !== "" && $_POST["mdp_ins"] !== "" ){
        $mdp_ins = password_hash($_POST["mdp_ins"], PASSWORD_BCRYPT);
        $image = @imagecreatefrompng('../stock/PP/bonhomme-removebg-preview.png') or die("Cannot Initialize new GD image stream");
        $background = imagecolorallocate($image, random_int(0,255), random_int(0,255), random_int(0,255));
        imagefill($image, 0, 0, $background);
        $resized = imagescale($image,60,60);
        $name = '../stock/PP/'.time().'.png';
        imagepng($resized,$name);
        addCoordonneesClient($pseudo_ins,$mail_ins,$num_ins,$mdp_ins,$name);
        $_SESSION["compte"] = ["pseudo" => $pseudo_ins,"client" => true];
        imagedestroy($resized);
        header("location:../RedirectAnimationPayment/animation.php");
        
    }
        }else{
        
        if (CheckExistClientName($pseudo_ins))  {
            header ('location: ../ConnexionPayment/Sing-up-login.php?error=le pseudo utilisateur existe déjà') ;
        } else if (CheckExistClientMail($mail_ins)) {
            header ('location: ../ConnexionPayment/Sing-up-login.php?error2=le mail utilisateur existe déjà') ;
        } else if (CheckExistClientPhone($num_ins)){
            header ('location: ../ConnexionPayment/Sing-up-login.php?error3=le phone utilisateur existe déjà') ;
        }
    }
}
    // retrait
    }else if ($retrait === true) {
    if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($mdp_ins) < 8) {
            header ('location: ../ConnexionPayment/Sing-up-login.php?error4=Le mot de passe doit comporter au moins 8 caractères et doit inclure au moins une lettre majuscule, un chiffre et un caractère spécial.') ;
        }else{
        if (CheckExistClientName($pseudo_ins) === false && CheckExistClientMail($mail_ins) === false && CheckExistClientPhone($num_ins) === false ){
        if ($_POST["pseudo_ins"] !== "" && $_POST["mdp_ins"] !== "" ){
            
            $mdp_ins = password_hash($_POST["mdp_ins"], PASSWORD_BCRYPT);
            $image = @imagecreatefrompng('../stock/PP/bonhomme-removebg-preview.png') or die("Cannot Initialize new GD image stream");
            $background = imagecolorallocate($image, random_int(0,255), random_int(0,255), random_int(0,255));
            imagefill($image, 0, 0, $background);
            $resized = imagescale($image,60,60);
            $name = '../stock/PP/'.time().'.png';
            imagepng($resized,$name);
            addCoordonneesClient($pseudo_ins,$mail_ins,$num_ins,$mdp_ins,$name);
            $_SESSION["compte"] = ["pseudo" => $pseudo_ins,"client" => true];
            imagedestroy($resized);
            header("location:../RedirectAnimationPayment/animation.php");
    }
        }else{
        
        if (CheckExistClientName($pseudo_ins))  {
            header ('location: ../ConnexionPayment/Sing-up-login.php?error=le pseudo utilisateur existe déjà') ;
        } else if (CheckExistClientMail($mail_ins)) {
            header ('location: ../ConnexionPayment/Sing-up-login.php?error2=le mail utilisateur existe déjà') ;
        } else if (CheckExistClientPhone($num_ins)){
            header ('location: ../ConnexionPayment/Sing-up-login.php?error3=le phone utilisateur existe déjà') ;
        }
    }
}
}

    ?>