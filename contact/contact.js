var refreshButton = document.querySelector("i.refresh-captcha");
refreshButton.onclick = function () {
  document.querySelector(".captcha-image").src =
    "../controller/captcha.php?" + Date.now();
};

function checkForm(form) {
  if (!form.captcha.value.match(/^\d{5}$/)) {
    alert("Please enter the CAPTCHA digits in the box provided");
    form.captcha.focus();
    return false;
  }

  return true;
}

let button = document.getElementById("formSubmit");

// POUR NOM
function validateFn() {
  let nom = document.getElementById("nom").value;
  let message = document.getElementById("MessageNom");

  let inputFieldChecks = `<p class ="non">Le nom est requis</p>`;
  let inputFieldChecks2 = `<p class ="non">Votre nom doit comporter entre 3 et 20 caractères sans chiffre(s), sans espace</p>`;
  let inputFieldChecks3 = "&#x2705";

  if (nom.length == 0) {
    message.innerHTML = inputFieldChecks;
    button.disabled = true;
    button.style.background = "red";
    nom.ClassName = "nom";
    return false;
  } else if (!nom.match(/^[A-Za-z]{1,20}\S\D\b$/g)) {
    message.innerHTML = inputFieldChecks2;
    button.disabled = true;
    button.style.background = "red";
    return false;
  } else {
    message.innerHTML = inputFieldChecks3;
    button.disabled = false;
    button.style.background = "#34623f";
    return true;
  }
}
// POUR EMAIL
function validateEmail() {
  var email = document.getElementById("email").value;
  let MessageEmail = document.getElementById("MessageEmail");

  let inputFieldChecks4 = `<p class ="non">L'adresse email est requise</p>`;
  let inputFieldChecks5 = `<p class ="non">Votre e-mail doit avoir ce format example@gmail.com`;
  let inputFieldChecks6 = "&#x2705";

  if (email.length == 0) {
    MessageEmail.innerHTML = inputFieldChecks4;
    button.disabled = true;
    button.style.background = "red";
    return false;
  } else if (
    !email.match(
      /^[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*@([a-z0-9]+([a-z0-9-]*)\.)+[a-z]+$/g
    )
  ) {
    MessageEmail.innerHTML = inputFieldChecks5;
    button.disabled = true;
    button.style.background = "red";
    return false;
  } else {
    MessageEmail.innerHTML = inputFieldChecks6;
    button.disabled = false;
    button.style.background = "#34623f";
    return true;
  }
}
// POUR COMMENT
function validateComment() {
  let comment = document.getElementById("comment").value;
  let MessageComment = document.getElementById("MessageComment");

  let inputFieldChecks7 = `<p class ="non">La commentaire est requis</p>`;
  let inputFieldChecks8 = `<p class ="non">Seulement des lettres</p>`;
  let inputFieldChecks9 = "&#x2705";

  if (comment.length == 0) {
    MessageComment.innerHTML = inputFieldChecks7;
    button.disabled = true;
    button.style.background = "red";
    comment.ClassName = "comment";
    return false;
  } else if (!comment.match(/^[a-zA-Z ]*$/)) {
    MessageComment.innerHTML = inputFieldChecks8;
    button.disabled = true;
    button.style.background = "red";
    return false;
  } else {
    MessageComment.innerHTML = inputFieldChecks9;
    button.disabled = false;
    button.style.background = "#34623f";
    return true;
  }
}
