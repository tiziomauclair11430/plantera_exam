<?php include "../header/header.php"; ?>
<h2 class="king">Boutique</h2>


<link rel="stylesheet" href="../boutique/boutique.css">
<link rel="stylesheet/less" type="text/css" href="soldeAnimation.less" />
<?php
    session_start();
    
    //  include "../debug/debug.php";
    include "../BDD/data.php";
    
        $list_pro = getProduit();
        $solde = getProduitAndImageAndSolde()
       

?>

<div id="sorting-bar">
    <select id="cat" class="input-product">
        <option value="0" id="aucun">sélectionner par catégorie</option>
        <option value="4" id="solde">Solde</option>
    <?php foreach(selectCategorie() as $select){
        echo '<option value="'.$select['id'].'">'.$select['nom'].'</option>';
    }?>
    </select>
</div>
<div class="avant-page" id="list">
    <?php $i=0;
    foreach($list_pro as $select){
        if($select['stock'] !== 0){
    ?>
    <div class="product-card" id="<?php echo $i?>">
        <a class="card-description" href="../view_product/view_product.php?product=<?php echo $select['id'] ?>">
            <img src="../stock/uploads/<?php echo getimage($select['id'])[0]['url']?>" class="img-product">
            <div class="description">
                <h3 class="titre"><?php echo $select['nom'] ?></h3>
                <p><?php echo $select['descriptionMini'] ?></p>
        </a>
        <p class="prix">
            <?php if($select['si_solde'] == 1){
                        echo $select['prix']."$";
                        }else{
                            print_r ('<h6 class="prix" style="text-decoration-line: line-through;">'.$select['prix'].'$</h6>') & print_r ('<h6 class="prix" style="color:red !important">New price! '.$select['prix_solde'].'$</h6>');}?>
        </p>
        <input type="hidden" name="cat" value="<?php echo catId($select['id'])[0]['id_categorie']?>">
        <input type="hidden" name="price" value="<?php echo $select['prix'] ?>">
    </div>
</div>
<?php $i++;} } ?>
</div>


<div id="cardanimation-tdv">
    <div class="card-container">
        <div class="card">
            <div class="card__face card__face--front front1">S</div>
            <div class="card__face card__face--back back1"><span>Jusqu'à</span></div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front2">O</div>
            <div class="card__face card__face--back back2">-</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front3">L</div>
            <div class="card__face card__face--back back3">6</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front4">D</div>
            <div class="card__face card__face--back back4">5</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front5">E</div>
            <div class="card__face card__face--back back5">%</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front6">S</div>
            <div class="card__face card__face--back back6">!</div>
        </div>
    </div>
</div>



<div class="avant-page" id="solde">
    <?php foreach($solde as $s){ ?>
    <div class="product-card">
        <a class="card-description" href="../view_product/view_product.php?product=<?php echo $s['id'] ?>">
        <img src="../stock/uploads/<?php echo $s['url']?>" class="img-product">
        <div class="description">
                <h3 class="titre"><?php echo $s['nom'] ?></h3>
                <p><?php echo $s['descriptionMini'] ?></p>
        </a>
            <p class="prix">
            <?php if($s['si_solde'] == 0){
                        
                            print_r ('<h6 class="prix" style="text-decoration-line: line-through;">'.$s['prix'].'$</h6>') & print_r ('<h6 class="prix" style="color:red !important">New price! '.$s['prix_solde'].'$</h6>');}?>
            </p>
        </div>
    </div>            
<?php } ?>
</div>
<script src="https://cdn.jsdelivr.net/npm/less@4"></script>
<script src="../boutique/boutique.js"></script>

<?php include "../footer/footer.php"; ?>