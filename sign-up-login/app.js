let btnLogin = document.getElementById("login");
let btnSignUp = document.getElementById("signup");

let signIn = document.querySelector(".signin");
let signUp = document.querySelector(".signup");

btnLogin.onclick = function () {
  signIn.classList.add("active");
  signUp.classList.add("inActive");
};

btnSignUp.onclick = function () {
  signIn.classList.remove("active");
  signUp.classList.remove("inActive");
};

// EYE connexion - PASSWORD CHECK
const togglePassword = document.querySelector("#togglePassword");
const password = document.querySelector("#mdp_ins");

togglePassword.addEventListener("click", function (e) {
  // toggle the type attribute
  const type =
    password.getAttribute("type") === "password" ? "text" : "password";
  password.setAttribute("type", type);
  // toggle the eye slash icon
  this.classList.toggle("fa-eye-slash");
});

// EYE login - PASSWORD CHECK
const togglePassword2 = document.querySelector("#togglePassword2");
const password2 = document.querySelector("#mdp_con");

togglePassword2.addEventListener("click", function (e) {
  // toggle the type attribute
  const type =
    password2.getAttribute("type") === "password" ? "text" : "password";
  password2.setAttribute("type", type);
  // toggle the eye slash icon
  this.classList.toggle("fa-eye-slash");
});

//popup
document.querySelector("#show-login").addEventListener("click", function () {
  document.querySelector(".popup").classList.add("active");
});
document
  .querySelector(".popup .close-btn")
  .addEventListener("click", function () {
    document.querySelector(".popup").classList.remove("active");
  });
