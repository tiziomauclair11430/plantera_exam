<!DOCTYPE html>
<html>
<?php 
    // include '../header/header.php';
session_start();
if(isset($_SESSION['compte']['client']) && !isset($_SESSION['compte']['admin'])){
    header("location:../client/espace_client.php#dashboard");
}else if(isset($_SESSION['compte']['admin']) && !isset($_SESSION['compte']['client'])){
    header("location:../admin/espace_admin.php#dashboard");
}
$_GET['error']
?>


<head>
    <title>Login Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="styles.css">
    
</head>
<body>
<script>
  document.body.style.zoom=0.9;
</script>

<a id="myBtn" class="fixed-button wobble" href="../home/index.php"><i class="fa-solid fa-house"></i></a>

    <div class="wrapper">
        
        <div class="headline">


            <h1>Plantera</h1>
        </div>
        <!-- Inscription-->
        <form class="form" method="post" action="../controller/connexion_inscription.php">
            <div class="signup">
                <div class="form-group">
                    <?php 
                    if($_GET['error']){
                        echo "<label style='color:#d62525'>".$_GET['error']."</label>";
                    }
                    ?>
                    <input class="pseudo_ins" type="text" placeholder="Login" required="" name="pseudo_ins"
                        id="pseudo_ins" <?php echo $_GET['error'];?>>
                </div>
                <?php 
                    if($_GET['error2']){
                        echo "<label style='color:#d62525'>".$_GET['error2']."</label>";
                    }
                    ?>
                <div class="form-group">
                    <input type="email" placeholder="Email" required="" name="mail_ins" id="mail_ins">
                </div>
                <?php 
                    if($_GET['error4']){
                        echo "<label style='color:#d62525'>".$_GET['error4']."</label>";
                    }
                    ?>
                <div class="form-group" id="passwordForm">
                    <input type="password" placeholder="Password" required="" name="mdp_ins" id="mdp_ins">
                    <div class="test">
                        <div class="show.hide">
                            <i class="fa-solid fa-eye" id="togglePassword" style=" cursor: pointer;"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="help">
                    <?php 
                    if($_GET['error3']){
                        echo "<label style='color:#d62525'>".$_GET['error3']."</label>";
                    }
                    ?>
                    <input type="tel" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" placeholder="Telephone" required=""
                        name="num_ins" id="num_ins">
                    <div class="test">
                        <div class="help-tip">
                            <i class="fa-solid fa-circle-question" id="question" data-hover="Format: 1234567890"
                                style=" cursor: pointer;"></i>
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn">SIGN UP</button>
                <div class="account-exist">
                    Vous avez déjà un compte? <a href="#" id="login">Login</a>
                </div>
            </div>
        </form>


        <!-- connexion-->

        <form class="form" method="post" action="../controller/connexion_control.php">
            <div class="signin">
                <div class="form-group">
                <?php 
                    if($_GET['test']){
                        echo "<label style='color:#d62525'>".$_GET['test']."</label>";
                    }
                    ?>
                    <input type="text" placeholder="Login" required="" name="pseudo_con" id="pseudo_con">
                </div>
                <div class="form-group" id="passwordForm">
                    <input type="password" placeholder="Password" required="" name="mdp_con" id="mdp_con">
                    <div class="test">
                        <div class="show.hide">
                            <i class="fa-solid fa-eye" id="togglePassword2" style=" cursor: pointer;"></i>
                        </div>
                    </div>
                </div>
                <div class="forget-password">
                    <div class="check-box">
                        <input type="checkbox" id="checkbox">
                        <label for="checkbox">Remember me</label>
                    </div>
                    <a id="show-login" href="#">Forget password?</a>
                </div>
                <button type="submit" class="btn">LOGIN</button>
        </form>
        <form class="form" method="post" action="../controller/reset-password.php">
            <div class="popup">
                <div class="close-btn">&times;</div>
                <div class="form">
                    <h2>Reset password</h2>
                    <div class="form-element">
                        <label for="email">Email</label>
                        <input type="text" required="" name="reset-password" id="email" placeholder="Enter email">
                    </div>
                    <div class="form-element">
                        <button type="submit"> Reset password</button>
                    </div>
                </div>
            </div>
        </form>


        <div class="account-exist">
            Créer un nouveau compte? <a href="#" id="signup">Signup</a>
        </div>
    </div>
    </div>

    </div>

    <script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
    <script src="app.js"></script>

</body>

</html>

