<?php include "../header/header.php"; ?>
<link rel="stylesheet" href="view_actualite.css">

<?php 
        include "../debug/debug.php";
       include "../BDD/data.php";
        $actualite_view = actualite_view();
      
      ?>
<?php
            foreach($actualite_view as $f){
        ?>
<div class="js-cover-image cover-image">
    <img src="../stock/uploads/<?php echo $f['photo']?>" alt="The Sea" />
</div>

<body>
    <main>

   
        <div class="containerAcctualite">

            <article>
                <h1><?php echo $f['title'];?></h1>
                <div class="metadata">
                    <span class="author"><?php echo $f['author'];?></span>
                    <span class="date"><?php echo $f['Ladate'];?></span>
                </div>
                <p><?php echo $f['paragraph1'];?></p>
                <p>
                    <?php echo $f['paragraph2'];?></p>

                <p><?php echo $f['paragraph3'];?></p>
                <!-- <div class="projcard-tagbox">
                    <span class="projcard-tag"><?php echo $f['mots_cles1'];?></span>
                    <span class="projcard-tag"><?php echo $f['mots_cles2'];?></span>
                    <span class="projcard-tag"><?php echo $f['mots_cles3'];?></span>

                </div> -->
            </article>
        </div>
        <?php } ?>
        <div id="disqus_thread"></div>
        <script>
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document,
                s = d.createElement('script');
            s.src = 'https://plantera.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
        </script>
        <!-- <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by
                Disqus.</a></noscript> -->
    </main>
    <script id="dsq-count-scr" src="//plantera.disqus.com/count.js" async></script>
</body>


<?php include "../footer/footer.php"; ?>